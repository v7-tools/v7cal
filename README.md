# v7cal




## Description
v7cal is the calendar program (`/bin/cal`) found in UNIX V7, but updated for modern C compilers.

## Usage
This program is designed as a porting tool for UNIX V7. One will have more fun porting it than using it.

## License
v7cal is licensed under the 2-clause BSD License.
